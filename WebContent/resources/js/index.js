var inicio = new Vue({
	el:"#inicio",
    data: {
        lista: [],
		novoFuncionario: {
			id: "",
    		nome: "",
    		setor: {nome: ""},
    		salario: "",
    		email: "",
    		idade: "",
		},
    },
    created: function(){
        let vm =  this;
        vm.listarFuncionarios();
    },
    methods:{
	//Busca os itens para a lista da primeira página
        listarFuncionarios: function(){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios")
			.then(response => {vm.lista = response.data;
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
		},
		
		deleteFuncionario: function(id){
			axios.delete("/funcionarios/rs/funcionarios/"+id)
			.then(response => {
				console.log("enviado")
			}).catch(function (error) {
				vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			}).finally(function() {
			});
			
			this.listarFuncionarios(); 
		},

		editar: function(funcionario) {
			this.novoFuncionario = funcionario;
		},
		
		atualizaFuncionario: function() {
			const vm = this
			let funcionario = JSON.parse(JSON.stringify(this.novoFuncionario))
			 axios.put("/funcionarios/rs/funcionarios", funcionario)
			 .then(response => {console.log("para atualizar")}).catch(function (error) {
				 vm.mostraAlertaErro("Erro interno", "Não foi listar natureza de serviços");
			 }).finally(function() {
			 });
			 
 			this.novoFuncionario = {
				id: " ",
				nome: " ",
				setor: {nome: " "},
				salario: " ",
				email: " ",
				idade: " ",
			 };

			 this.listarFuncionarios(); 

 
		},
    }
});