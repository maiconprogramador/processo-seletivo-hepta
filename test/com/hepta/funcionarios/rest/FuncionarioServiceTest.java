package com.hepta.funcionarios.rest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.util.List;
import java.util.function.BooleanSupplier;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.hepta.funcionarios.entity.Funcionario;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.FuncionarioDAO;

class FuncionarioServiceTest {
	
	private FuncionarioDAO dao;
	
	public FuncionarioServiceTest() {
		dao = new FuncionarioDAO();
	}
		
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
	}
	
	@Test
	void testFuncionarioRead() throws Exception {
		System.out.println("Running testList...");
	     
	    List<Funcionario> listaDeFuncionario = dao.getAll();
	     
	    Assertions.assertFalse(listaDeFuncionario.isEmpty());
	}

	@Test
	void testFuncionarioCreate() {
		System.out.println("Running testCreate...");
		
		Funcionario funcionario = new Funcionario();
		Setor setor = new Setor();
		setor.setNome("TI");
		funcionario.setNome("Maicon");
		funcionario.setSetor(setor);
		funcionario.setEmail("maicon@gmail.com");
		funcionario.setSalario(1.500);
		funcionario.setIdade(20);
		    
	}

	@Test
	void testFuncionarioUpdate() throws Exception {
		System.out.println("Running testUpdate...");
		
		Funcionario funcionario = new Funcionario();
		Setor setor = new Setor();
		setor.setId(1);
		setor.setNome("TI");
		funcionario.setId(4);
		funcionario.setNome("Maicon");
		funcionario.setSetor(setor);
		funcionario.setEmail("maicon@gmail.com");
		funcionario.setSalario(1.500);
		funcionario.setIdade(20);
		
		dao.update(funcionario);
		
		assertEquals("Maicon", funcionario.getNome());		
		
	}

	@Test
	void testFuncionarioDelete() throws Exception {
		 	System.out.println("Running testDelete...");

		    Integer id = 9;
		    dao.delete(id);
		    Funcionario funcionarioDeletado = dao.find(id);
		    
		    Assertions.assertNull(funcionarioDeletado);     
		    
	}

}
